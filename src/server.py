#!/usr/bin/python3

import RPi.GPIO as GPIO
import time
import paho.mqtt.client as mqtt

broker_address = "192.168.0.107"

reedswitch17 = 17
reedswitch15 = 15

# Create new instance
client = mqtt.Client("P1")

# Connect to the MQTT broker
print("connecting to broker")
client.connect(broker_address)

# Set the pins used to GPIO style rather than board pins
GPIO.setmode(GPIO.BCM)

# Setup the pins using the names declared, set them as inputs and use a software pullup resistor
GPIO.setup(reedswitch17, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(reedswitch15, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# Fuction that is called on every event change for the GPIO pins with interupts set
def reedswitch_callback(answer):
    # Example: home-assistant/sensor-pi-1/reedswitch/17
    topic = "home-assistant/sensor-pi-1/reedswitch/" + str(answer)
    if GPIO.input(answer):
        # Example: Door on GPIO 17: Open
        print("Door on GPIO {}: Open".format(answer))
        client.publish((topic),"OPEN")
    else:
        # Example: Door on GPIO 17: Closed
        print("Door on GPIO {}: Closed".format(answer))
        client.publish((topic),"CLOSED")

# Runs when the script starts to set the correct states
reedswitch_callback(15)
reedswitch_callback(17)

# Create the interupts used to call the reedswitch_callback function and pass though the GPIOpin
# as (answer) using a debounce time of 500ms
GPIO.add_event_detect(reedswitch17, GPIO.BOTH, callback=reedswitch_callback, bouncetime=500)
GPIO.add_event_detect(reedswitch15, GPIO.BOTH, callback=reedswitch_callback, bouncetime=500)

# Keep script alive and sent heartbeats to HAC to let it know it's alive
while True:
    print('heatbeat')
    client.publish("home-assistant/sensor-pi-1/availability","ONLINE")
    time.sleep(60)

# GPIO cleanup
GPIO.remove_event_detect(reedswitch17)
GPIO.remove_event_detect(reedswitch15)
GPIO.cleanup()
