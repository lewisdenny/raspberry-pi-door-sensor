#!/usr/bin/python3

import RPi.GPIO as GPIO
import time
import paho.mqtt.client as mqtt
import dht11
import json

broker_address="192.168.0.107" 
client = mqtt.Client("P2")
client.connect(broker_address)

# initialize GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

#keep script alive and sent heartbeats to HAC to let it know its alive
while True:
    
    # read data using pin 14
    instance = dht11.DHT11(pin = 14)
    result = instance.read()

    if result.is_valid():
       
        payload = {
            "temperature": result.temperature,
            "humidity": result.humidity
        }

        payload_json = json.dumps(payload)  # Convert dict to json string
        print(payload_json)

        client.publish("home-assistant/roof/sensor/climate1",(payload_json))
    else:
        print("Error: %d" % result.error_code)
        #client.publish("home-assistant/sensor-pi-1/availability","ONLINE")
    time.sleep(60)

#GPIO cleanup
GPIO.cleanup()
